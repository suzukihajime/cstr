#! /usr/bin/env python
# -*- coding: utf-8 -*-

# shared library test runner

from ctypes import *

def parse_args():
	# check sanity of and parse arguments
	import argparse

	p = argparse.ArgumentParser(
		description = 'cstr: C shared library based unittest runner.')
	
	p.add_argument('libname',
		metavar = '<libname>',
		type = str,
		help = 'name of the shared library which contains unittest functions.')
	p.add_argument('--prefix', '-p',
		type = str,
		help = 'prefix of the unittest functions.',
		default = 'test_',
		required = False)

	a = p.parse_args()
	return(a.libname, a.prefix)

def format_libname(lib):
	# format library name for the environment
	
	import re
	import sys

	p = re.compile('^lib')
	s = re.compile('.[a-z]+$')

	if sys.platform == 'darwin':
		prefix = 'lib'
		suffix = '.dylib'
	elif sys.platform.startswith('linux'):
		prefix = 'lib'
		suffix = '.so'

	if lib.find('-') != -1:
		return None
	dll = s.sub(suffix, p.sub(prefix, lib))
	return dll

def get_fullpath(libname, libpath_list = ['.']):
	# convert full path of the library

	import os

	libpath = libpath_list + ['/lib', '/usr/lib']
	if 'LD_LIBRARY_PATH' in os.environ:
		libpath += os.environ['LD_LIBRARY_PATH'].split(':')
	for path in libpath:
		tpath = os.path.join(path, libname)
		if os.path.exists(tpath):
			return os.path.abspath(tpath)
	return None

def load_lib(libname):
	# load library using cdll
	return cdll.LoadLibrary(get_fullpath(libname))

def list_symbols(lib):

	import subprocess

	path = get_fullpath(lib._name)
	outputs = subprocess.check_output(['nm', str(format(path))]).decode().split('\n')
	symbols = [m[2] for m in [l.split() for l in outputs] if len(m) == 3 and m[1] == 'T']
	return symbols

# cdll.__getitem__でポインタを取得
def get_function(lib, symbols, prefix):
	prefix = prefix.lstrip('_')
	funcs = [lib.__getitem__(symbol.lstrip('_')) for symbol in symbols if symbol.lstrip('_').startswith(prefix)]
	return funcs

# テストを走らせる
def run(libname, unittest_prefix = 'test_'):

	# load shared library
	libname = format_libname(libname)
	lib = load_lib(libname)

	if get_fullpath(lib._name) == None:
		print('invalid library')
		return None

	# load symbols from library
	symbols = list_symbols(lib)
	funcs = get_function(lib, symbols, unittest_prefix)

	# run tests
	lib.cstr_init()
	for func in funcs:
		func()

	# get summary
	test = len(funcs)
	fail = lib.cstr_get_fail()
	succ = lib.cstr_get_succ()
	total = fail + succ

	# cleanup
	lib.cstr_init()

	return({'test_count': test, 'assertion_count': total, 'fail': fail, 'succ': succ})

# 結果の表示
def print_result(result):
	print('Summary: total {} assertions in {} tests, {} success, {} fail.'.format(
		result['assertion_count'],
		result['test_count'],
		result['succ'],
		result['fail']))

# run all
if __name__ == '__main__':
	
	# check arguments
	(libname, prefix) = parse_args()
	print('Running unittests on `{}\'.'.format(libname))

	# run tests
	result = run(libname, prefix)
	print_result(result)

