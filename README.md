
cstr -- C Shared-library-based Test Runner
==========

Description
----------

This is a collection of a python script and C support functions,
which enables auto collection of assertion based unittest functions
and summarization of results.

This shared-library-based method does not require the C source code to 
use macros, removing conflicts with your own macros.

Platform
----------
Only Mac OS X is supported for now.

Usage
----------
```
$ gcc -shared -fPIC -o libtest.dylib cstr.c test.c
$ ./cstr.py libtest.dylib
```

