
#include <stdio.h>
#include <stdlib.h>
#include "cstr.h"

void test_1(void)
{
	assert(1 == 1);
	return;
}

void test_2(void)
{
	assert(2 == 2);
	return;
}

void test_3(void)
{
	assert(3 == 4);
	return;
}

void test_4(void)
{
	assert(3 != 4);
	return;
}
