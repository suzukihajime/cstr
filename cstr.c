
/**
 * @file cstr.c
 *
 * @brief collection of the unittest utilities
 */
#include <stdint.h>

/* global variables */
int64_t cstr_g_fail;
int64_t cstr_g_succ;

void cstr_init(void)
{
	cstr_g_fail = 0;
	cstr_g_succ = 0;
	return;
}

void cstr_add_fail(void)
{
	cstr_g_fail++;
	return;
}

void cstr_add_succ(void)
{
	cstr_g_succ++;
	return;
}

int64_t cstr_get_fail(void)
{
	return(cstr_g_fail);
}

int64_t cstr_get_succ(void)
{
	return(cstr_g_succ);
}

/**
 * end of cstr.c
 */
