
/**
 * @file cstr.h
 *
 * @brief function declarations of cstr utilities
 */

#ifndef _CSTR_H_INCLUDED
#define _CSTR_H_INCLUDED

#ifdef assert

/* assert.h detected */
#warning "assert.h in the standard C library is included and cstr_assert is suppressed. remove #include <assert.h> if you want to use cstr unittest utilities."

#else

#include <stdint.h>
#include <stdio.h>

/**
 * @macro assert
 * @brief assert (compatible to assert in the standard C)
 */
#define assert(x) { \
	if(x) { /* suc */ \
		cstr_add_succ(); \
	} else { \
		fprintf(stderr, "assertion failed: %s:l%d (%s) < %s >\n", __FILE__, __LINE__,  __func__, #x); \
		cstr_add_fail(); \
	} \
}

void cstr_init(void);
void cstr_add_fail(void);
void cstr_add_succ(void);
int64_t cstr_get_fail(void);
int64_t cstr_get_succ(void);

#endif	/* assert */

#endif	/* _CSTR_H_INCLUDED */
/**
 * end of cstr.h
 */
